import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed, async, fakeAsync, tick} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {Observable} from 'rxjs';
import {Book} from '../store/book/book.model';
import {BookService} from '../store/book/book.service';

import {BookComponent} from './book.component';

describe('BookComponent', () => {
  let component: BookComponent;
  let fixture: ComponentFixture<BookComponent>;
  let de: DebugElement;
  let el: HTMLElement;
  let bookService: BookService;
  let bookSpy: any;

  const bookData: Book[] = [
    {title: 'angular-testing 1', author: 'Stefan Martinek'},
    {title: 'angular-testing 2', author: 'Stefan Martinek'},
    {title: 'angular-testing 3', author: 'Stefan Martinek'},
    {title: 'angular-testing 4', author: 'Stefan Martinek'}
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BookComponent],
      providers: [BookService]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    el = de.nativeElement;
  });

  beforeEach(() => {
    bookService = fixture.debugElement.injector.get(BookService);
    bookSpy = spyOn(bookService, 'query').and.returnValue(Observable.of(bookData));
  });

  it('should not show books before OnInit', () => {
    const bookEls: DebugElement[] = de.queryAll(By.css('#book-list .book-item'));
    expect(bookEls.length).toBe(0, 'no books yet');
    expect(bookSpy.calls.any()).toBe(false, 'query not yet called');
  });

  it('should still not show books after component initialized', () => {
    const bookEls: DebugElement[] = de.queryAll(By.css('#book-list .book-item'));
    fixture.detectChanges();
    expect(bookEls.length).toBe(0, 'no books yet');
    expect(bookSpy.calls.any()).toBe(true, 'query called');
  });

  it('should show books after query promise (async)', async(() => {
    bookSpy.calls.any();
    fixture.detectChanges();
    fixture.whenStable().then(() => {   // wait for async getQuote
      fixture.detectChanges();
      const bookEls: DebugElement[] = de.queryAll(By.css('#book-list .book-item'));
      expect(bookEls.length).toBeGreaterThan(0, 'no books yet');
      expect(bookSpy.calls.any()).toBe(true, 'query called');
    });
  }));

  it('should show books after query promise (fakeAsync)', fakeAsync(() => {
    fixture.detectChanges();
    tick();                  // wait for async getQuote
    fixture.detectChanges(); // update view with quote
    const bookEls: DebugElement[] = de.queryAll(By.css('#book-list .book-item'));
    expect(bookEls.length).toBeGreaterThan(0, 'no books yet');
  }));

});
