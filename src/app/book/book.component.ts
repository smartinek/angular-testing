import {Component, OnInit} from '@angular/core';
import {Book} from '../store/book/book.model';
import {BookService} from '../store/book/book.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  books: Book[];

  constructor(private bookService: BookService) {
  }

  ngOnInit() {
    this.bookService.query().subscribe((books: Book[]) => {
      this.books = books;
    });
  }
}
