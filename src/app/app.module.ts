import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';


import {AppComponent} from './app.component';

import {BookComponent} from './book/book.component';
import {HeaderComponent} from './header/header.component';
import {BookService} from './store/book/book.service';
import {UserService} from './store/user/user.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BookComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [UserService, BookService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
