import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';


@Injectable()
export class BookService {

  constructor() {
  }

  query() {
    return Observable.of([
      {title: 'Book 1', author: 'Max'}
    ]);
  }

}
