import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export class UserService {

  isLoggedIn = false;
  user = null;

  constructor() {
  }

  get() {
    return Observable.of({isLoggedIn: true, user: {name: 'Stefan Martinek'}});
  }

}
