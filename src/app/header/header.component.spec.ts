import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {UserService} from '../store/user/user.service';

import {HeaderComponent} from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let header: DebugElement;
  let el: HTMLElement;

  let userService;
  let welcome: DebugElement;


  // stub UserService for test purposes
  const userServiceStub = {
    isLoggedIn: true,
    user: {name: 'Test User'}
  };

  // WebPack developers need not call compileComponents because it inlines templates and css as
  // part of the automated build process that precedes running the test.
  /**
   beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent]
    }).compileComponents();
    **/

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [{provide: UserService, useValue: userServiceStub}]
    });

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance; // HeaderComponent test instance

    userService = TestBed.get(UserService);

    // fixture.debugElement is handler to the component's DOM element
    // By.css is a static method that filters the same way as a jQuery selector
    const debugElement = fixture.debugElement.query(By.css('.header'));
    header = debugElement.query(By.css('h1'));
    welcome = debugElement.query(By.css('#welcome'));
  });

  it('the user should be welcomed', () => {
    fixture.detectChanges();
    const content = welcome.nativeElement.textContent;
    expect(content).toContain('Welcome', '"Welcome ..."');
    expect(content).toContain('Test User', 'expected name');
  });

  it('should welcome "Bubba"', () => {
    userService.user.name = 'Bubba'; // welcome message hasn't been shown yet
    fixture.detectChanges();
    const content = welcome.nativeElement.textContent;
    expect(content).toContain('Bubba');
  });

  it('should request login if not logged in', () => {
    userService.isLoggedIn = false; // welcome message hasn't been shown yet
    fixture.detectChanges();
    const content = welcome.nativeElement.textContent;
    expect(content).not.toContain('Welcome', 'not welcomed');
    expect(content).toMatch(/log in/i, '"log in"');
  });

  it('the header should display the title', () => {
    // detectChanges triggers data binding
    fixture.detectChanges();
    expect(header.nativeElement.textContent).toContain(component.title);
  });

  it('no title in the DOM until manually call `detectChanges`', () => {
    expect(header.nativeElement.textContent).toEqual('');
  });

  it('the header should display another title', () => {
    component.title = 'another title for angular-testing';
    fixture.detectChanges();
    expect(header.nativeElement.textContent).toContain('another title for angular-testing');
  });

});
