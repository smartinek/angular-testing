import {Component, OnInit} from '@angular/core';
import {UserService} from '../store/user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  title = 'angular-testing';
  welcome: string;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.welcome = this.userService.isLoggedIn ?
      'Welcome, ' + this.userService.user.name :
      'Please log in.';
  }

}
